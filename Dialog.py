import json
class Response(object):
	def __init__(self, node):
		self.Parent = node
		self.CharNeeded = 1
		self.Text = "Default"
		self.Script = ""
		self.id = 0
		self.Next = None
		self.Link = -1#link to another node (not tree)
	def get_json(self):
		return {'CharNeeded':self.CharNeeded,
				'Script':self.Script,
				'Next':self.Next,
				'Link':self.Link,
				'id':self.id,
				'Text':self.Text}

class Node(object):
	def __init__(self, id, diag):
		self.Diag = diag
		self.id = id
		self.NPCText = "DefaultNPC"
		self.Responses = []
	def get_json(self):
		rs = [r.get_json() for r in self.Responses]
		return {'NPCText':self.NPCText, 'Responses':rs,'id':self.id}
		
class Dialog(object):
	def __init__(self):
		self.NPCID = ""
		self.Nodes = {}
		self.Responses = {}
	def save(self, path):
		j = {'NPCID':self.NPCID}
		nodes = {self.Nodes[n].id: self.Nodes[n].get_json() for n in self.Nodes}
		j['Nodes'] = nodes
		s = json.dumps([j])
		f = open(path,'w')
		f.write(s)
		f.close()
	def load(self, json):
		"""should take a parsed json representing
		the dialog as input"""
		self.__init__()
		self.NPCID = json['NPCID']
		print json
		for node_id in json['Nodes']:
			n = json['Nodes'][node_id]
			node = Node(n['id'],self)
			node.NPCText = n['NPCText']
			for response in n['Responses']:
				r = Response(n)
				r.Script = response['Script']
				r.CharNeeded = response['CharNeeded']
				r.Text = response['Text']
				r.id = int(response['id'])
				self.Responses[r.id] = r
				r.Next = response['Next']
				r.Link = response['Link']
				node.Responses.append(r)
			self.Nodes[node.id] = node




		