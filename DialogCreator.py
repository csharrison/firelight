from Tkinter import *
from Dialog import *

def getText(textNode):
	s = textNode.get(1.0,END)
	s = s[:len(s)-1]
	return s

class NodeMaker(object):
	def __init__(self,dialog):
		self.id = 0
		self.dialog = dialog
	def make_node(self, list, dict):
		self.id += 1
		new = Node(self.id,self.dialog)
		list.append(new)
		dict[new.get_key()] = new
		return new

class DialogMaker:
	def __init__(self,parent):
		self.node_dict = {}
		self.responses = []
		self.dialog = Dialog()
		self.node_maker = NodeMaker(self.dialog)
		self.cur_node = self.node_maker.make_node(self.dialog.Nodes,self.node_dict)
		self.cur_response = Response(self.cur_node)
		self.cur_node.Responses.append(self.cur_response)
				
		self.parent = parent
		#sets root as containers parent
		self.container = Frame(parent)

		#set up node list
		self.add_node_button = Button(self.container, text="Add NPC Nodes")
		self.add_node_button.grid(row=1,column=0)
		self.add_node_button.bind("<Button-1>", self.add_new_node)

		self.nodes_listbox = Listbox(self.container,selectmode=SINGLE)
		self.nodes_listbox.bind("<Double-Button-1>",self.update_cur_node)
		self.nodes_listbox.grid(row=2,column=0)
		self.container.pack()

		self.npc_id_label = Label(self.container,text="NPCID")
		self.npc_id_label.grid(row=3,column=0)
		self.npc_id = Entry(self.container)
		self.npc_id.grid(row=4,column=0)

		self.write = Entry(self.container)
		self.write.insert(END,"C:\\Firelight\\Squashed\\assets\\Dialogs\\default.JSON")
		self.write.grid(row=5,column=0)

		self.write_button = Button(self.container,text="Write to file")
		self.write_button.grid(row=6,column=0)
		self.write_button.bind("<Button-1>", self.write_to_file)


		#set up node info
		self.npc_text_label = Label(self.container,text="NPC Text")
		self.npc_text_label.grid(row=1,column=1)
		self.npc_text = Text(self.container,width=50,height=7)
		self.npc_text.grid(row=2,column=1)

		self.save_button = Button(self.container,text="Save All")
		self.save_button.grid(row=4,column=1)
		self.save_button.bind("<Button-1>", self.save)


		self.response_listbox_button = Button(self.container,text="Add Response")
		self.response_listbox_button.grid(row=1,column=2)
		self.response_listbox_button.bind('<Button-1>', self.add_new_response)

		self.response_listbox = Listbox(self.container,selectmode=SINGLE)
		self.response_listbox.bind("<Double-Button-1>",self.update_cur_response)
		self.response_listbox.grid(row=2,column=2)

		self.response_text_label = Label(self.container,text="Response Text")
		self.response_text_label.grid(row=1, column=3)
		self.response_text = Text(self.container,width=40,height=7)
		self.response_text.grid(row=2,column=3)

		self.response_script_label = Label(self.container,text="Response Script (json)")
		self.response_script_label.grid(row=3,column=3)
		self.resonse_script = Text(self.container,width=40,height=7)
		self.resonse_script.grid(row=4,column=3)

		self.response_char_label = Label(self.container, text="Charisma Needed (1-10)")
		self.response_char_label.grid(row=5,column=3)
		self.response_char = Entry(self.container)
		self.response_char.insert(END,"1")
		self.response_char.grid(row=6,column=3)

		self.response_next_label = Label(self.container, text="Next Node (id)")
		self.response_next_label.grid(row=7,column=3)
		self.response_next = Entry(self.container)
		self.response_next.insert(END,"0")
		self.response_next.grid(row=8,column=3)

		self.update()
	def add_new_node(self,e):
		new = self.node_maker.make_node(self.dialog.Nodes, self.node_dict)
		self.update()
	def add_new_response(self,e):
		new = Response(self.cur_node)
		self.cur_node.Responses.append(new)
		self.update()

	def update_cur_response(self,e):
		key = self.response_listbox.curselection()[0]
		self.cur_response = self.cur_node.Responses[int(key)]
		self.update()

	def update_cur_node(self,e):
		key = self.nodes_listbox.curselection()[0]
		self.cur_node = self.dialog.Nodes[int(key)]
		self.update()
	def save(self,e):
		self.cur_node.NPCText = self.npc_text.get(1.0,END)
		self.cur_response.Text = self.response_text.get(1.0,END)
		self.cur_response.Script = self.resonse_script.get(1.0,END)
		self.cur_response.CharNeeded = int(self.response_char.get())
		self.cur_response.Next = int(self.response_next.get())
		self.dialog.NPCID = self.npc_id.get()

		print self.write.get()
		self.update()
	def write_to_file(self,e):
		self.dialog.save(self.write.get())
	#updates visuals
	def update(self):
		#update the nodes dropdown
		self.nodes_listbox.delete(0,END)
		for node in self.dialog.Nodes:
			self.nodes_listbox.insert(END, node.get_key())

		self.response_listbox.delete(0,END)
		for response in self.cur_node.Responses:
			self.response_listbox.insert(END, response.Text)

		self.npc_text.delete(1.0,END)
		self.npc_text.insert(END, self.cur_node.NPCText)

		#delete the text
		self.response_text.delete(1.0,END)
		self.response_text.insert(END,self.cur_response.Text)
		#delete the script
		self.resonse_script.delete(1.0,END)
		self.resonse_script.insert(END,self.cur_response.Script)

		self.response_char.delete(0)
		self.response_char.insert(END,str(self.cur_response.CharNeeded))
		self.response_next.delete(0)
		self.response_next.insert(END,str(self.cur_response.Next))
		self.container.pack()
		
if __name__ == "__main__":
	root = Tk()
	creator = DialogMaker(root)
	root.mainloop()