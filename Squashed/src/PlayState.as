package
{
	import com.adobe.utils.XMLUtil;
	import net.pixelpracht.tmx.TmxMap;
	import net.pixelpracht.tmx.TmxObject;
	import org.flixel.*;
 	import net.pixelpracht.tmx.TmxObjectGroup;

	public class PlayState extends FlxState
	{
		public var level:FlxTilemap;
		public var player:Player;
		public var paused:FlxPaused;
		[Embed(source="../maps/map01.tmx",mimeType = "application/octet-stream" ) ] private var MapTmx:Class;
		[Embed(source="../maps/tiles.png")] private var ImgTiles:Class;
		override public function create():void
		{
			/*
			FlxG.bgColor = 0xffaaaaaa
			level = new Map();
			add(level);
			//Create player (a red box)
			
			Registry.player = player;
			ai = new AI(300, 200);
			
			var hud:FlxGroup = new FlxGroup();
			text = new FlxText(8, 8, 100, "hud");
			text.setFormat(null, 14);
			hud.add(text);
			hud.setAll("scrollFactor", new FlxPoint(0, 0));
			add(player);
			add(ai);
			//add(dialog);
			add(text);
			
			add(hud);
			//add(paused);
			*/
			FlxG.mouse.show();
			Registry.playState = this;
			
			
			
			paused = new FlxPaused();
			
			var xml:XML = new XML(new MapTmx());
			var l:TmxMap  = new TmxMap(xml);
			loadStateFromTmx(l);
			

		}
		private function loadStateFromTmx(tmx:TmxMap):void
		{
			FlxG.bgColor = 0xffacbcd7;
			var group:TmxObjectGroup = tmx.getObjectGroup('objects');
			for each(var object:TmxObject in group.objects)
			{
				spawnObject(object);
			}
			
			//Basic level structure
			var t:FlxTilemap = new FlxTilemap();
			//generate a CSV from the layer 'map' with all the tiles from the TileSet 'tiles'
			var mapCsv:String = tmx.getLayer('map').toCsv(tmx.getTileSet('tiles'));
			t.loadMap(mapCsv, ImgTiles);
			level = t;
			//t.follow();

			add(t);
			FlxG.camera.setBounds(0, 0, t.width, t.height);
			FlxG.worldBounds = FlxG.camera.bounds;
		}
		private function spawnObject(obj:TmxObject):void
		{
			switch(obj.type)
			{
				case "player":
					player = new Player(obj.x, obj.y);
					add(player);
					FlxG.camera.follow(player, FlxCamera.STYLE_LOCKON);
					return;
				case "ai":
					var ai:AI = new AI(obj.x, obj.y, obj.custom['NPCID']);
					add(ai);
					return;
			}
		}
		override public function update():void
		{	
			if (! paused.showing)
			{
				super.update();
				FlxG.collide(level, player);
				//FlxG.collide(level, ai);
			}
			else {
				paused.update();
			}
		}
		public function showDialog(npc:AI):void {
			paused = new FlxPaused;			
			paused.showPaused(npc);
			add(paused);
		}


	}
}