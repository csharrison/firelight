package 
{
	import org.flixel.*;
	public class Player extends Ball 
	{
		
		public function Player(x:Number = 0, y:Number = 0)
		{
			super(x, y);
			weapon = new Gun(100);
		}
		public function maxSpeed() : void {
			//normalize
			var x:Number = velocity.x;
			var y:Number = velocity.y;
			var hypot:Number = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
			x = x / hypot;
			y = y / hypot;
			velocity.x = x * maxVel;
			velocity.y = y * maxVel;
		}
		override public function update():void
		{
			var mouseX:Number = FlxG.mouse.x;		//Get the X position of the mouse in the game world
			var mouseY:Number = FlxG.mouse.y;
			var screenX:Number = FlxG.mouse.screenX;	//Get the X position of the mouse in screen space

			var pressed:Boolean = FlxG.mouse.pressed();	//Check whether the mouse is currently pressed
		 
			var justPressed:Boolean = FlxG.mouse.justPressed();
		 
			var justReleased:Boolean = FlxG.mouse.justReleased();
			
			var dirx:Number = mouseX - x-15;
			var diry:Number = mouseY - y-10;
			
			pointTowards(mouseX - 15, mouseY - 10);
			//acceleration.x = 0;
			//acceleration.y = 0;
			var hypot:Number = Math.sqrt(Math.pow(dirx, 2) + Math.pow(diry, 2));
						
			dirx = dirx / hypot;
			diry = diry / hypot;
			
			//velocity.x = velocity.x/2;
			//velocity.y = velocity.y/2;
			if (FlxG.keys.W) {
					velocity.x += dirx*maxVel/3;
					velocity.y += diry*maxVel/3;
			}
			if (FlxG.keys.S) {
					velocity.x += -dirx*maxVel/3;
					velocity.y += -diry*maxVel/3;
				//acceleration.x = -maxVelocity.x * dirx * 4 ;
				//acceleration.y = -maxVelocity.y * diry * 4;
			}
			if (FlxG.keys.A) {
					velocity.x += diry*maxVel/4;
					velocity.y += -dirx*maxVel/4;
				//acceleration.x = maxVelocity.y * diry * 4 ;
				//acceleration.y = -maxVelocity.x * dirx * 4 ;
			}
			if (FlxG.keys.D) {
					velocity.x += -diry*maxVel/4;
					velocity.y += dirx*maxVel/4;
				//acceleration.x = -maxVelocity.y * diry * 4 ;
				//acceleration.y = maxVelocity.x * dirx * 4 ;
			}
			var speed:Number = Math.sqrt(Math.pow(velocity.x, 2) + Math.pow(velocity.y, 2));
			if (speed > maxVel) {
				maxSpeed();
			}
			if (FlxG.keys.SPACE)
			{
				weapon.pressDownTrigger();
				
			}
			else {
				weapon.letGoOfTrigger();
			}
			if (justPressed)
			{
				var level:FlxTilemap = Registry.playState.level;
				trace(x+" "+y+ " "+ mouseX+ " "+mouseY);
				var path:FlxPath = level.findPath(new FlxPoint(x, y), new FlxPoint(mouseX, mouseY));
				path.removeAt(0);
				path.removeAt(0);
				this.followPath(path);
			}
			weapon.update(this);
			super.update();
		}
	}
	
}