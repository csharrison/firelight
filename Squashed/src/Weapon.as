package 
{
	import org.flixel.*;
	
	public interface Weapon 
	{
		function shoot(ball:Ball):void;
		
		function pressDownTrigger():void;
		function letGoOfTrigger():void;
		function update(ball:Ball):void;
	}
	
}