package
{

import org.flixel.*;
import com.adobe.serialization.json.JSONDecoder;
import flash.utils.ByteArray;

public class FlxPaused extends FlxGroup
{


/**
* Background rect for the text
*/
private var _bg:FlxSprite;

/**
* The text field used to display the text
*/
private var fields:Array =[];
/**
* Use this to tell if dialog is showing on the screen or not.
*/
public var showing:Boolean;

internal var _displaying:Boolean;

/**
 * Called when dialog is finished (optional)
 */
private var _finishCallback:Function;

public var Dialogs:Object={};
public var text:FlxText;
public var choices:Array = [];
public var NPCTalking:Boolean = true;
public var curDialog:Dialog;
[Embed(source = "../assets/Dialogs/first.JSON",mimeType="application/octet-stream")]
private const JsonData:Class;

public function FlxPaused()
{
		var width:Number = FlxG.width;
		var height: Number = 75;
		var y:Number = (FlxG.height - height);
		_bg = new FlxSprite(0,y).makeGraphic(width, height, 0xaa808080);
		_bg.scrollFactor.x = _bg.scrollFactor.y = 0;
		add(_bg);
		
		var b:ByteArray = new JsonData();
		var s:String = b.readUTFBytes(b.length);
		//load all the dialogs
		var c:Object = new JSONDecoder(s, true).getValue();
		var i:Number = 0;
		while (i in c)
		{
			var dialog:Dialog = new Dialog(c[i]);
			Dialogs[dialog.NPCID] = dialog;
			i = i + 1;
		}
		
}

/**
* Call this from your code to display some dialog
*/
public function showPaused(npc:AI):void
{
	NPCTalking = true;
	curDialog = Dialogs[npc.NPCID];
	curDialog.startDialog();
	displayText();
	
	_displaying = true;
	showing = true;
}
public function stopDialog():void
{
	this.kill();
	trace('heya');
	this.exists = false;
	showing = false;
	_displaying = false;
	if (_finishCallback != null) _finishCallback();
}
/**
* The meat of the class. Used to display text over time as well
* as control which page is 'active'
*/
override public function update():void
{
	if (_displaying)
	{
		
		if (curDialog)
		{
			if (FlxG.keys.justPressed("SPACE"))
			{
				curDialog.advance();
				displayText();
			}
			else if (curDialog.chosen)
			{
				if (! curDialog.done)
				{
					displayText();
					curDialog.chosen = false;
				}
				else {
					this.stopDialog();
					curDialog = null;
				}
			}
		}
		
		
	super.update();
	}	
}
public function displayText():void
{
	
	for (var i:Number = 0; i < fields.length; i++)
	{
		remove(fields[i]);
	}
	fields = curDialog.getDialog();
	for (i = 0; i < fields.length; i++)
	{
		//txts is a flxtext-like object
		var t:FlxText = fields[i];
		trace(t);
		t.x = _bg.x;
		t.y = _bg.y + i * 10;
		add(t);
	}
}

/**
		 * Called when the dialog is completely finished
		 */
		public function set finishCallback(val:Function):void
		{
			_finishCallback = val;
		}

}
}
