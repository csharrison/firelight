package 
{
	public class Quest 
	{
		class QuestNode
		{
			public var parent:Quest;
			public var next:Array;
			public var description:String;//can be used in the journal
			public function QuestNode(q:Quest)
			{
				parent = q;
				next = [];
				description = "";
			}
		}
		public var name:String;
		
		public function Quest() 
		{
			
		}
	}	
}