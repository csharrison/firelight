package 
{
	import org.flixel.*;
	
	public class Node extends FlxText
		{
			public var id:Number;
			public var NPCText:String;
			public var Responses:Array = [];
			public var dialog:Dialog;
			public function Node(d:Dialog, json_node:Object) {
				
				dialog = d;
				id = json_node['id'];
				NPCText = json_node['NPCText'];
				var responses:Object = json_node['Responses'];
				var i :Number = 0;
				while (i in responses)
				{
					var r:Response = new Response(this, responses[i]);
					Responses.push(r);
					i = i + 1;
				}
				super( -1000, -1000, FlxG.width, NPCText);
				scrollFactor.x = scrollFactor.y = 0;
				setFormat(null, 12, 0xff00FFFF, "center");
			}
		}
	
}