package 
{
	import com.adobe.utils.ArrayUtil;
	import com.adobe.utils.NumberFormatter;
	import org.flixel.FlxPoint;

	public class Gun implements Weapon 
	{
		public var bulletPool:Array;
		public var bulletsShot:Array;
		private var timeBetweenShots:Number=10;
		private var shooting:Boolean;
		public var bulletSpeed:Number=300;
		private var timer:Number= 0;
		public function Gun(bulletPoolSize:Number):void
		{
			//initialize a bullet pool to gather bullets from
			bulletsShot = [];
			bulletPool = [];
			for (var i:Number = 0 ; i < bulletPoolSize; i++)
			{
				bulletPool.push(new Bullet());
			}
		}
		public function pressDownTrigger():void
		{
			shooting = true;
		}
		public function letGoOfTrigger():void
		{
			if (shooting) {
				shooting = false;
				timer = 5;			
			}
			
		}
		public function shoot(ball:Ball):void
		{
			if (bulletPool.length > 0)
			{
				var bullet:Bullet = bulletPool.pop();
				bulletsShot.push(bullet);
				bullet.shoot(ball, bulletSpeed);
				Registry.playState.add(bullet);
			}
		}
		public function update(ball:Ball):void
		{	
			if (shooting)
			{
				if(timer <= 0)
				{
					shoot(ball);
					timer = timeBetweenShots;
				}
				timer = timer - 1;		
			}
			var i:Number = 0;
			while (i < bulletsShot.length)
			{
				var b:Bullet = bulletsShot[i];
				if (b.timer <= 0)
				{
					bulletsShot.splice(i, 1);
					bulletPool.push(b);
					b.goBackToPool();
					i = i - 1;
				}
				i++;
			}
		}
	}
	
}