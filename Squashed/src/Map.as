package 
{
	import org.flixel.FlxTilemap;
	import flash.utils.ByteArray;

	public class  Map extends FlxTilemap
	{
		[Embed(source = "../../mapmaker/map1", mimeType = "application/octet-stream")]
		private const mapData:Class;
		public var npcs:Array = [];
		public function Map()
		{
			var b:ByteArray = new mapData();
			var s:String = b.readUTFBytes(b.length);
			super();
			loadMap(s, FlxTilemap.ImgAuto, 0, 0, FlxTilemap.AUTO);
		}
	}
	
}