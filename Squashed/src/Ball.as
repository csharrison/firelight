package 
{
	import com.adobe.utils.NumberFormatter;
	import org.flixel.*;
	import Math;
	public class Ball extends FlxSprite
	{
	[Embed(source = "../assets/ship3.png")]
	private var CrystalReflSheet:Class;
	public var radius:Number;
	public var weapon:Weapon;
	public var maxVel:Number = 100;
	private var level:FlxTilemap;
		public function Ball(x:Number=0, y:Number=0)
		{
			super(x, y); // location
			drag.x = maxVel * 4;
			drag.y = maxVel *4;
			loadGraphic(CrystalReflSheet, false, false);
			radius = 15;
		}
		public function pointTowards(xl:Number, yl:Number):Boolean
		{
			var dirx:Number = xl - x;
			var diry:Number = yl - y;
			
			var Dangle:Number = Math.atan2(diry, dirx) / (Math.PI / 180) + 90;
			//find if we're going left or right
			if (Dangle-angle  > 180) {//we're going to go left
				Dangle = Dangle - 360;
				angle = (Dangle-angle) / 6 + angle;
				angle = angle + 360;
				return Math.abs(Dangle - angle -360) > 5;
			}
			else if (Dangle - angle < - 180)
			{
				Dangle = Dangle + 360;
				angle = (Dangle-angle) / 6 + angle;
				angle = angle - 360;
				return Math.abs(Dangle - angle + 360) > 5;
			}
			else {
				angle = (Dangle-angle) / 6 + angle;
				return Math.abs(Dangle - angle) > 5;
			}
		}
	}	
}