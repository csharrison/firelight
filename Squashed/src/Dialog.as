package 
{	
	import com.adobe.utils.ArrayUtil;
	import com.adobe.utils.StringUtil;
	import org.flixel.*;
	public class  Dialog
	{
		public var Nodes:Object={};
		public var NPCID:String;
		public var done:Boolean;
		public var curNode:Node;
		public var chosen:Boolean = false;
		public var responding:Boolean = false;
		public function Dialog(json:Object) {
			NPCID = json['NPCID'];
			var json_list:Object = json['Nodes'];
			var i:Number = 1;
			while(i in json_list)
			{
				var n:Node = new Node(this,json_list[i]);
				Nodes[n.id] = n;
				i = i + 1;
			}
		}
		public function startDialog():void
		{
			curNode = Nodes[1];
		}
		public function getDialog():Array
		{
			if (responding)
			{
				return curNode.Responses;
			}
			else {
				return new Array(curNode);
			}
		}
		public function advance(choice:Response=null):void
		{
			if (responding)
			{
				var script : String = choice.Script;
				//TODO: evaluate script
				var link : Number = choice.Link;
				if (link == 0)
				{
					done = true;
				}
				else if (link != -1)
				{
					curNode = Nodes[link];
				}
				else {
					curNode = Nodes[choice.Next];
				}
				chosen = true;
			}
			else {
				var responseList:Array = curNode.Responses;
			}
			responding = ! responding;
		}
	}
	
}