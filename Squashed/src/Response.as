package 
{
	import org.flixel.*;
	
	public class Response extends FlxText
	{
		public var Parent:Node;
		public var CharNeeded:Number = 1;
		public var Text:String;
		public var Script:String;
		public var Link:Number;
		public var Next:Number = 1;
		public function Response(node:Node, json:Object) {
			
			
			Parent = node;
			Text = json['Text'];
			Next = json['Next'];
			Link = parseInt(json['Link']);
			Script = json['Script'];
			super( -1000, -1000, FlxG.width, Text);
			scrollFactor.x = scrollFactor.y = 0;
			setFormat(null, 12, 0xff00FFFF, "center");
		}
		override public function update():void
		{
			var screenX:Number = FlxG.mouse.screenX;	//Get the X position of the mouse in screen space
			var screenY:Number = FlxG.mouse.screenY;	//Get the X position of the mouse in screen space
			if (overlapsPoint(new FlxPoint(screenX, screenY)))
			{
				color = 0x00aaaaaa;
				if (FlxG.mouse.justPressed())
				{
					Parent.dialog.advance(this);
				}
			}
			else {
				color = 0x00111111;
			}
			super.update();
		}
	}

	
}