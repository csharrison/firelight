package 
{
	import com.adobe.utils.ArrayUtil;
	import com.adobe.utils.NumberFormatter;
	import org.flixel.*;

	public class Bullet extends FlxSprite
	{
		[Embed(source = "../assets/pp_small.png")]
		private var bulletImage:Class;
		public var timer:Number = 100;
		public var shot:Boolean = false;
		public function Bullet()
		{
			super(-1000, -1000);
			loadGraphic(bulletImage);
		}
		public function goBackToPool():void
		{
			x = -1000;
			y = -1000;
			velocity.x = 0;
			velocity.y = 0;
			shot = false;
			timer = 100;
		}
		public function shoot(ball:Ball, speed:Number):void
		{
			shot = true;
			x = ball.x;
			y = ball.y;
			var a:Number = (ball.angle-90) * (Math.PI) / 180;
			velocity.x = Math.cos(a) * speed + ball.velocity.x/5;
			velocity.y = Math.sin(a) * speed + ball.velocity.y/5;
		}
		override public function update():void
		{
			if(shot){timer = timer - 1}
			super.update();
		}
	}
	
}