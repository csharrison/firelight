package 
{
	import com.adobe.utils.StringUtil;
	import org.flixel.*;
	import flash.display.Shape;
	public class AI extends Ball 
	{
		public var isHovering:Boolean = false;
		public var pointing:Boolean = false;
		public var attacking:Boolean = false;
		public var NPCID:String = "Default";
		public function AI(x:Number = 0, y:Number = 0, id:String ="Default")
		{
			super(x, y);
			NPCID = id;
		}
		override public function update():void
		{
			var mouseX:Number = FlxG.mouse.x;		//Get the X position of the mouse in the game world
			var mouseY:Number = FlxG.mouse.y;
			var pressed:Boolean = FlxG.mouse.pressed();	//Check whether the mouse is currently pressed
			var justPressed:Boolean = FlxG.mouse.justPressed();
			
			if (this.overlapsPoint(new FlxPoint(mouseX, mouseY)))
			{
				isHovering = true;
				if (pressed || justPressed ||pointing)
				{
					var player:Player = Registry.playState.player;
					pointing = pointTowards(player.x, player.y);
					if (! pointing)//towards him now
					{
						Registry.playState.showDialog(this);
					}
				}
			}
			else {
				isHovering = false;
			}
			super.update();
		}
		override public function draw():void {
			if (isHovering) {
				//var myShape:Shape = new Shape();
				//myShape.graphics.lineStyle(1);
				//myShape.graphics.drawCircle(x+radius,y+radius,radius);
				//FlxG.camera.buffer.draw(myShape);
			}
			super.draw();

		}

	}
	
}