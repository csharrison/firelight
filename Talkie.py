
from Dialog import Dialog
from json import loads
def between(num, min, max):
	return num <= max and num >= min
def load_all_dialog(file_path):
		f = open(file_path,'r')
		r = f.read()
		f.close()
		j = loads(r)
		dialogs = []
		for dialog_json in j:
			d = Dialog()
			d.load(dialog_json)
			dialogs.append(d)
		return dialogs
def converse(dialog):
	main = dialog.Nodes[1]
	while main != None:
		print main.NPCText
		exec(main.Script)
		for i,response in enumerate(main.Responses):
			r = eval(response.BeforeScript)
			print str(i)+". " + r
			print ""
		r_choice = int(raw_input(">>> "))
		while not between(r_choice, 0, len(main.Responses)-1):
			r_choice = int(raw_input(">>> "))

		r = main.Responses[r_choice]
		index = eval(r.AfterScript)
		if index != -1:
			main = dialog.Nodes[index]
		else: break

if __name__ == "__main__":
	dialogs = load_all_dialog("Default.JSON")
	converse(dialogs[0])