import pygame as pg
from pygame.locals import *
def save(filename,grid):
	m = ""
	for y in grid:
		row = str(y)
		m += row[1:len(row)-1] + "\n"
	f = open(filename,'w')
	f.write(m)
	f.close()

def reset(screen,grid,block_size):
	for x in xrange(len(grid)):
		for y in xrange(len(grid[x])):
			if grid[x][y]==1:
				pg.draw.rect(screen, (255,0,0), (x*block_size,y*block_size,block_size,block_size))
		pg.display.update();

def main():
	pg.init()
	dim = (1920,1080)
	block_size = 10

	grid = [[0 for x in xrange(dim[0]/block_size)] for y in xrange(dim[1]/block_size)]
	#dim = (1366,768)
	screen = pg.display.set_mode(dim,pg.FULLSCREEN)
	mouseDown = False
	while True:
		mx,my=[i for i in pg.mouse.get_pos()]
		for e in pg.event.get():
			if e.type == KEYDOWN:
				if e.key == K_ESCAPE: pg.quit();  sys.exit()
				elif e.key == K_d: save('map1',grid)
				elif e.key == K_SPACE: reset(screen,grid,block_size)
			elif e.type == KEYUP: pass
			elif e.type == MOUSEBUTTONDOWN:
				mouseDown = True
			elif e.type == MOUSEBUTTONUP:
				mouseDown = False
		if mouseDown:
			cellx,celly = mx/block_size,my/block_size
			grid[celly][cellx] = 1
			pg.draw.rect(screen, (255,0,0), (cellx*block_size,celly*block_size,block_size,block_size))
		pg.display.update();
	
if __name__ == "__main__":
	main();