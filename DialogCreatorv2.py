from Tkinter import *
from Dialog import *
import ttk
import json
def getText(textNode):
	s = textNode.get(1.0,END)
	s = s[:len(s)-1]
	return s

class NodeMaker(object):
	def __init__(self,dialog):
		self.node_id = 0
		self.response_id = 0
		self.dialog = dialog
	def make_node(self, dict):
		self.node_id += 1
		new = Node(self.node_id,self.dialog)
		dict[(new.id)] = new
		return new
	def make_response(self, node, dict):
		self.response_id += 1
		new = Response(node)
		new.id = self.response_id;
		node.Responses.append(new)
		dict[(self.response_id)] = new
		return new
class DialogMaker:
	def new_node(self):
		self.cur_node = self.node_maker.make_node(self.dialog.Nodes)
	def new_response(self):
		self.cur_response = self.node_maker.make_response(self.cur_node, self.dialog.Responses)
	def __init__(self,parent):

		self.dialog = Dialog()
		self.node_maker = NodeMaker(self.dialog)
		self.cur_node = self.node_maker.make_node(self.dialog.Nodes)
		self.cur_response = None

		self.parent = parent
		#sets root as containers parent
		self.container = Frame(parent)

		self.tree = ttk.Treeview(self.container)
		tree = self.tree
		self.tree.grid(row=3,column=0, rowspan=5,columnspan=2,
						ipadx=200,ipady=100)
		# Inserted at the root, program chooses id:
		tree.insert('', 'end', 'node_1', text=self.cur_node.NPCText)
		tree.bind("<Double-Button-1>", self.select)

		self.save_button = Button(self.container, text='save')
		self.save_button.grid(row = 0, column = 4)
		self.save_button.bind('<Button-1>', self.save)

		self.new = Button(self.container, text='new child')
		self.new.grid(row = 1, column = 4)
		self.new.bind('<Button-1>', self.new_child)


		#set up node info
		self.npc_text_label = Label(self.container,text="NPC Text")
		self.npc_text_label.grid(row=2,column=4)
		self.npc_text = Text(self.container,width=30,height=3)
		self.npc_text.grid(row=3,column=4)

		#set up response info
		self.response_text_label = Label(self.container,text="Response Text")
		self.response_text_label.grid(row=2,column=4)
		self.response_text = Text(self.container,width=30,height=3)
		self.response_text.grid(row=2,column=5)	

		self.response_script_label = Label(self.container,text="Response Script")
		self.response_script_label.grid(row=3,column=4)
		self.response_script = Text(self.container,width=30,height=3)
		self.response_script.grid(row=3,column=5)	


		self.rcha_l = Label(self.container,text="Charisma needed")
		self.rcha_l.grid(row=4,column=4)
		self.rcha = Entry(self.container)
		self.rcha.insert(END,'1')
		self.rcha.grid(row=4,column=5)	

		self.link_l = Label(self.container,text="link")
		self.link_l.grid(row=5,column=4)
		self.link = Entry(self.container)
		self.link.grid(row=5,column=5)
		self.link.insert(END, '-1')

		self.load = Entry(self.container)
		self.load.insert(END,"C:\\Firelight\\Squashed\\assets\\Dialogs\\default.JSON")
		self.load.grid(row=0,column=0)

		self.load_button = Button(self.container,text="Load this to file")
		self.load_button.grid(row=0,column=1)
		self.load_button.bind("<Button-1>", self.load_file)


		self.write = Entry(self.container)
		self.write.insert(END,"C:\\Firelight\\Squashed\\assets\\Dialogs\\default.JSON")
		self.write.grid(row=1,column=0)

		self.write_button = Button(self.container,text="Write to file")
		self.write_button.grid(row=1,column=1)
		self.write_button.bind("<Button-1>", self.write_to_file)



		self.npc_id_label = Label(self.container,text="NPCID")
		self.npc_id_label.grid(row=2,column=0)
		self.npc_id = Entry(self.container)
		self.npc_id.grid(row=2,column=1)

		self.show_node()
		self.container.pack()
		self.select(None,'1')
	def write_to_file(self,e):
		self.dialog.save(self.write.get())
	def show_response(self):
		self.npc_text_label.grid_remove()
		self.npc_text.grid_remove()

		self.response_text.grid()
		self.response_text_label.grid()
		self.response_script.grid()
		self.response_script_label.grid()
		self.rcha_l.grid()
		self.rcha.grid()
		self.link_l.grid()
		self.link.grid()

	def show_node(self):
		self.response_text.grid_remove()
		self.response_text_label.grid_remove()
		self.response_script.grid_remove()
		self.response_script_label.grid_remove()
		self.rcha_l.grid_remove()
		self.rcha.grid_remove()
		self.link_l.grid_remove()
		self.link.grid_remove()

		self.npc_text_label.grid()
		self.npc_text.grid()
	def getText(self,textNode):
		s = textNode.get(1.0,END)
		s = s[:len(s)-1]
		return s
	def save(self,e=None):
		self.dialog.NPCID = self.npc_id.get()
		self.cur_node.NPCText = self.getText(self.npc_text)
		self.tree.item('node_'+str(self.cur_node.id),text=self.cur_node.NPCText)

		if self.cur_response:
			self.cur_response.Text = self.getText(self.response_text)
			self.tree.item('response_'+str(self.cur_response.id),text=self.cur_response.Text)

			self.cur_response.Script = self.getText(self.response_script)
			self.cur_response.CharNeeded = self.rcha.get()
			self.cur_response.Link = self.link.get()

	def new_child(self, e):
		self.save()
		
		id = self.tree.selection()[0]
		print id
		type,num = id.split('_')
		if(type=="node"): 

			self.new_response()
			self.tree.insert('node_' + str(self.dialog.Nodes[int(num)].id), 'end',  'response_'+str(self.cur_response.id), text=self.cur_response.Text)

		elif type=="response": 
			self.new_node()
			self.cur_response.Next = self.cur_node.id
			self.tree.insert('response_'+str(self.dialog.Responses[int(num)].id), 'end', 'node_' + str(self.cur_node.id), text=self.cur_node.NPCText)

	def select(self,event, node_id=None):
		if(node_id):
			id = node_id
			type,num = 'node',int('1')
		else:
			id = self.tree.selection()[0]
			print id
			type,num = id.split('_')
		if type == "node":
			self.show_node();
			#reset curnode to selected node

			self.cur_node = self.dialog.Nodes[int(num)]
			print self.cur_node
			#populate nodes with text for the new node

			self.npc_text.delete(1.0,END)
			self.npc_text.insert(END, self.cur_node.NPCText)
		elif type == "response":
			self.show_response();
			self.cur_response = self.dialog.Responses[int(num)]

			self.response_text.delete(1.0,END)
			self.response_text.insert(END, self.cur_response.Text)
			self.response_script.delete(1.0,END)
			self.response_script.insert(END, self.cur_response.Script)

			self.rcha.delete(0,END)
			self.rcha.insert(END, self.cur_response.CharNeeded)

			self.link.delete(0,END)
			self.link.insert(END, self.cur_response.Link)
	def clear(self):
		print self.tree.get_children()
		while len(self.tree.get_children()) > 0:
			self.tree.delete(self.tree.get_children())


	def load_file(self,e):
		path = self.load.get()
		f = open(path,'r')
		r = f.read()
		f.close()
		j = json.loads(r)[0]

		self.clear()

		self.dialog = Dialog()
		self.dialog.load(j)
		root = self.dialog.Nodes[1]
		self.tree.insert('','end','node_1', text=root.NPCText)
		for response in root.Responses:
			self.load_response(root, response)
				
	def load_node(self, parent_response, node):
		parentid = 'response_'+str(parent_response.id)
		id = 'node_'+ str(node.id)
		self.tree.insert(parentid, 'end',id, text=node.NPCText)
		self.node_maker.node_id = node.id
		for response in node.Responses:
			self.load_response(node, response)
	def load_response(self, parent_node, response):
		parentid = 'node_'+str(parent_node.id)
		id = 'response_'+str(response.id)
		self.tree.insert(parentid, 'end',id, text=response.Text)
		self.node_maker.response_id = response.id
		if response.Next:
			self.load_node(response, self.dialog.Nodes[response.Next])



if __name__ == "__main__":
	root = Tk()
	creator = DialogMaker(root)
	root.mainloop()